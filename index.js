const express = require('express')
const jwt = require("jsonwebtoken");
const passport = require("./lib/passport");
const authentication = require("./routers/authentication");
const gameplayRoom = require("./routers/gameplay");
const room = require("./routers/room");
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize())
app.use(authentication)
app.use(gameplayRoom)
app.use(room)

app.use((req,res) => {
  res.status(404).send("halaman tidak ditemukan")
})
///500
app.use((err,req,res,next) =>{
  res.status(500).send("Error on app")
  })
//-----

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})