'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class gameplay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.gameplay.belongsTo(models.Room,{
        foreignKey : "roomId",
      })
      models.gameplay.belongsTo(models.User,{
        foreignKey : "userId",
      });
      
    }
  }
  gameplay.init({
    roomId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'gameplay',
  });
  return gameplay;
};