
const { User } = require("../models");
const jwt = require('jsonwebtoken')

exports.registUser = async (req, res) => {
    await User.create(req.body)
    res.send("masukan berhasil")
  };

exports.loginPage = (req, res) => {
    res.send("Halaman Login")
  };


exports.loginSuccess = async (req,res) => {
    try {
        const user = await User.findOne({
          where: {
            username: req.body.username,
            password: req.body.password,
          },
        });
    
        if (user) {
          const payload = { uid: user.id };
          const secret = "sangat-rahasia";
    
          const token = jwt.sign(payload, secret);
          console.log(token)
          
          res.json({
            token: token,
          });
        } else {
          res.status(401).send("Invalid login!");
        }
      } catch (err) {
        console.log(err);
        res.status(500).send(err.message);
      }
    }
  